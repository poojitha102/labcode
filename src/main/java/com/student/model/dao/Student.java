package com.student.model.dao;

public class Student {
	private int id;
	private String fname;
	private String subname;
	private int marks;
	private int testnumber;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getSubname() {
		return subname;
	}

	public void setSubjectname(String subname) {
		this.subname = subname;
	}

	public int getMarks() {
		return marks;
	}

	public void setSubmarks(int marks) {
		this.marks = marks;
	}

	public int getTestnumber() {
		return testnumber;
	}

	public void setTestnumber(int testnumber) {
		this.testnumber = testnumber;
	}

	public Student(int id, String fname, String subjectname, int subjectmarks, int testnumber, String subname,
			int marks) {
		super();
		this.id = id;
		this.fname = fname;
		this.subname = subname;
		this.marks = marks;
		this.testnumber = testnumber;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", fname=" + fname + ", subname=" + subname + ", marks=" + marks + ", testnumber="
				+ testnumber + "]";
	}

}